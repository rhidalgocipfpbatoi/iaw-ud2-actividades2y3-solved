<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>IAW-UD2-A3-2</title>
</head>
<body>
<h1> Actividad 2 - Media aritmética</h1>

<?php
/**
 * Realiza un programa que calcule la media de tres notas almacenadas en 3 variales $num1, $num2 y $num3
 */

$num1 = 23;
$num2 = 50;
$num3 = 45;

$result = ($num1 + $num2 + $num3) / 3;

echo "<p><strong>($num1 + $num2 + $num3) / 3 = </strong>$result</p>";

?>

</body>
</html>