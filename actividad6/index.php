<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>IAW-UD2-A3-6</title>
    <link rel="stylesheet" type="text/css" href="../assets/css/style.css">
</head>
<body>

<h1> Actividad 6 - Euromillón</h1>

<!-- Se pide realizar un generador para una apuesta del juego de Euromillones que nos permita obtener al azar cinco números
principales (del 1 al 50) y dos números (del 1 al 9) llamados “estrellas”.
Las estrellas las pintaremos en verde mientras que los números en azul
-->
<?php
$num1 = mt_rand(0,9);
$num2 = mt_rand(10,19);
$num3 = mt_rand(20,29);
$num4 = mt_rand(30,39);
$num5 = mt_rand(40,50);

$estrella1 = mt_rand(0,4);
$estrella2 = mt_rand(5,9);

$numColor = "rgb(0,191,255)";
$starColor = "rgb(255,255,0)";
?>

<!-- Numeros principales -->
<svg version="1.1" xmlns="http://www.w3.org/2000/svg"
     width="100" height="100">
    <circle cx="50" cy="50" r="50" fill="<?=$numColor?>" />
    <text x="50%" y="65%" text-anchor="middle" style="fill: white; stroke: none; font-size: 48px;"><?=$num1?></text>
</svg>
<svg version="1.1" xmlns="http://www.w3.org/2000/svg"
     width="100" height="100">
    <circle cx="50" cy="50" r="50" fill="<?=$numColor?>" />
    <text x="50%" y="65%" text-anchor="middle" style="fill: white; stroke: none; font-size: 48px;"><?=$num2?></text>
</svg>
<svg version="1.1" xmlns="http://www.w3.org/2000/svg"
     width="100" height="100">
    <circle cx="50" cy="50" r="50" fill="<?=$numColor?>" />
    <text x="50%" y="65%" text-anchor="middle" style="fill: white; stroke: none; font-size: 48px;"><?=$num3?></text>
</svg>
<svg version="1.1" xmlns="http://www.w3.org/2000/svg"
     width="100" height="100">
    <circle cx="50" cy="50" r="50" fill="<?=$numColor?>" />
    <text x="50%" y="65%" text-anchor="middle" style="fill: white; stroke: none; font-size: 48px;"><?=$num4?></text>
</svg>
<svg version="1.1" xmlns="http://www.w3.org/2000/svg"
     width="100" height="100">
    <circle cx="50" cy="50" r="50" fill="<?=$numColor?>" />
    <text x="50%" y="65%" text-anchor="middle" style="fill: white; stroke: none; font-size: 48px;"><?=$num5?></text>
</svg>

<!-- Numeros Estrellas -->
<svg version="1.1" xmlns="http://www.w3.org/2000/svg"
     width="100" height="100">
    <circle cx="50" cy="50" r="50" fill="<?=$starColor?>" />
    <text x="50%" y="65%" text-anchor="middle" style="fill: white; stroke: none; font-size: 48px;"><?=$estrella1?></text>
</svg>

<svg version="1.1" xmlns="http://www.w3.org/2000/svg"
     width="100" height="100">
    <circle cx="50" cy="50" r="50" fill="<?=$starColor?>" />
    <text x="50%" y="65%" text-anchor="middle" style="fill: white; stroke: none; font-size: 48px;"><?=$estrella2?></text>
</svg>

</body>
</html>