<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>IAW-UD2-A3-5</title>
</head>
<body>
<h1> Actividad 5 - Operadores aritméticos</h1>
<?php

/**
 * Partiendo de 2 variables $primera y $segunda con valores 10 y 2 , respectivamente, hacer una página PHP que calcule:
 * 1. La diferencia de $primera menos $segunda.
 * 2. La división de $primera entre $segunda.
 */

$primera = 10;
$segunda2 = 2;

$diferencia = $primera - $segunda2;
$division = $primera / $segunda2;

echo "<li>
        <ul><strong>Diferencia: $diferencia</strong></ul>
        <ul><strong>División: $division</strong></ul>
     </li>"
?>
</body>
</html>