<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>IAW-UD2-A3-3</title>
</head>
<body>
<h1> Actividad 2 - Par / Impar</h1>

<?php

/**
 * Escribe un programa en el cual se declaran las variables enteras x e y. *Asígnale los valores 144 y 999 respectivamente.
 * A continuación, muestra por pantalla el valor de cada variable, la suma, el resto, la división y la multiplicación.
 * Cada operación irá en un parrafo diferente. Ej. <p><strong> Operación suma: </strong> resultado </p>
 */

$x = 56;
$i = 49;

$suma = $x + $i;
$resto = $x % $i;
$división = $x / $i;
$multiplicación = $x * $i;

echo "<p><strong>Operación suma: </strong> $suma </p>";
echo "<p><strong>Operación resto: </strong> $resto </p>";
echo "<p><strong>Operación división: </strong> $división </p>";
echo "<p><strong>Operación multiplicación: </strong> $multiplicación </p>";

?>

</body>
</html>