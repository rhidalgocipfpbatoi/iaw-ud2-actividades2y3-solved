<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>IAW - Implantación de Aplicaciones Web</title>
    <link rel="stylesheet" type="text/css" href="../assets/css/style.css">
</head>
<body>
<h1> Actividad 8 - Tabla precios productos</h1>
<?php

/**
 * Escribe un programa que calculo el precio final de un producto según su base imponible (precio antes de impuestos), teniendo en cuenta
 * que el IVA aplicado puede ser:
 * - General: 21%
 * - Reducido: 10%
 * Además pueden disfrutar de diferentes promociones:
 * - PROMO_MITAD: 50%
 * - PROMO_DIA: 5%;
 * Muestra en una tabla los diferentes precios que se pueden tener de un producto a partir de las combinaciones de IVA y promociones.
 */

define("IVA", 0.21);
define("IVA_REDUCIDO", 0.1);
define("PROMO_MITAD", 0.5);
define("PROMO_DIA", 0.05);

$precio = 110;
$precioIVANoPromo = $precio + ($precio * IVA);
$precioIVAPromoMitad = $precioIVANoPromo - $precioIVANoPromo * PROMO_MITAD;
$precioIVAPromoDia =  $precioIVANoPromo - $precioIVANoPromo * PROMO_DIA;

$precioIVAReducidoNoPromo = $precio + ($precio * IVA_REDUCIDO);
$precioIVAReducidoPromoMitad = $precioIVAReducidoNoPromo - $precioIVAReducidoNoPromo * PROMO_MITAD;
$precioIVAReducidoPromoDia =  $precioIVAReducidoNoPromo - $precioIVAReducidoNoPromo * PROMO_DIA;

echo "<table>
        <tr><th colspan='4'>Zapatillas: $precio €</th></tr>
        <tr><td>TIPO IVA</td><td>NO PROMO</td><td>PROMO MITAD</td><td>PROMO DIA</td></tr>
        <tr>
            <td><strong>IVA Normal</strong></td><td>$precioIVANoPromo</td><td>$precioIVAPromoMitad</td><td>$precioIVAPromoDia</td>
        </tr>
        <tr>
            <td><strong>IVA Reducido</strong></td><td>$precioIVAReducidoNoPromo</td><td>$precioIVAReducidoPromoMitad</td><td>$precioIVAReducidoPromoDia</td>
        </tr>"
?>
</body>
</html>