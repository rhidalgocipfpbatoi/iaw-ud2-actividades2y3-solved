<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>IAW-UD2-A3-4</title>
</head>
<body>
<!-- Escriba un programa que cada vez que se ejecute muestre un línea de longitud entre 10 y 1000 píxeles, al azar.
Utiliza la función mt_rand() -->
<h1> Actividad 4 - Línea de tiempo</h1>
<?php
    $longitud = mt_rand(10,1000);
?>
<p>Longitud: 525</p>
<p>
    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="<?=$longitud?>px" height="10px">
        <line x1="1" y1="5" x2="<?=$longitud?>" y2="5" stroke="black" stroke-width="10" />
    </svg>
</p>
</body>
</html>