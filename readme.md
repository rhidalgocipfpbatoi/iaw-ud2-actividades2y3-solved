# Conociendo el Lenguaje Php (I)

#### Actividad 1

Realiza un conversor de Mb a Kb. La cantidad en euros que se quiere convertir deberá estar
almacenada en una variable.

#### Actividad 2

Realiza un programa que calcule la media de tres notas almacenadas en 3 variales `$num1`, `$num2` y `$num3`.

#### Actividad 3

Escriu un programa en el qual es declaren les variables senceres x e i. Asígnali els valors 144 i 999 respectivament.
A continuació, mostra per pantalla el valor de cada variable, la suma, la resta, la divisió i la multiplicació.
Cada operació anirà en un paragraf diferent. Ex. <p><strong> Operacio suma: </strong> resultat </p>

#### Actividad 4
Escriba un programa que cada vez que se ejecute muestre un línea de longitud entre 10 y 1000 píxeles, al azar.
Utiliza la función [mt_rand()](https://www.php.net/manual/es/function.mt-rand.php).

#### Actividad 5
Partiendo de 2 variables $primera y $segunda con valores 10 y 2 , respectivamente, hacer una página PHP que calcule:
1. La diferencia de $primera menos $segunda.
2. La división de $primera entre $segunda.

#### Actividad 6

Se pide realizar un generador para una apuesta del juego de Euromillones que nos permita obtener al azar cinco números
principales (del 1 al 50) y dos números (del 1 al 9) llamados “estrellas”.
Las estrellas las pintaremos en verde mientras que los números en azul.

#### Actividad 7

Escriba un programa que cada vez que se ejecute muestre un círculo de 50px de radio y de un color elegido al azar.

#### Actividad 8
 Escribe un script que calcule el precio final de una zapatillas según su base imponible (precio antes de impuestos), teniendo en cuenta
 que:
 El tipo de IVA aplicable es: 
 - General: 21%
 - Reducido: 10%
 
 Los clientes pueden disfrutar de las siguientes promociones:
 - PROMO_MITAD: 50%
 - PROMO_DIA: 5%;
 
 Muestra en una tabla los diferentes precios que se pueden tener de un producto a partir de las combinaciones de IVA y promociones.

**Nota:** El tipo de IVA y los códigos promocionales deberán ser declarados como constantes ``*define`` *ó ``*const``.
El precio de las zapatillas se almacenará en una variables ``precio``.

La siguiente imagen muestra el resultado que queremos obtener:

![Drag Actividad9](assets/images/actividad9.png)
